
def bubbleSort(myArray):
    lenOfArray = len(myArray)
    for i in range(lenOfArray, 0, -1):
        for j in range(0, i-1):
            if(myArray[j+1] < myArray[j]):
                myArray[j+1], myArray[j] = myArray[j], myArray[j+1]
    return myArray


def partition(myArray, lowValue, highValue):
    '''
        myArray is the array to sort
        lowValue is the starting index
        highValue is the ending
    '''
    '''
        This function define the pivot
        In this function, I take the last element of my array as my pivot
        then I sort it so that it can be to right place
        by putting smaller element in the left
        and higher the right
    '''
    i = lowValue
    pivot = myArray[highValue]
    for j in range(lowValue, highValue):
        if (myArray[j] < pivot):
            '''
                I check if my value is smaller or higher than the pivot
            '''
            '''
                Then I increment his index
            '''
            permuteOne = myArray[i], myArray[j]
            permuteOne = myArray[j], myArray[i]
            i = i + 1
    permuteTwo = myArray[i], myArray[highValue]
    permuteTwo = myArray[highValue], myArray[i]
    return i


def quickSort(myArray, lowValue, highValue):
    if (lowValue < highValue):
        partitionIndex = partition(myArray, lowValue, highValue)
        quickSort(myArray, lowValue, partitionIndex-1)
        quickSort(myArray, partitionIndex+1, highValue)


def mergeSort(myArray):
    '''
        Splitting my array
    '''
    if len(myArray)>1:
       mid = len(myArray)//2
       lefthalf = myArray[:mid]
       righthalf = myArray[mid:]
       mergeSort(lefthalf)
       mergeSort(righthalf)

       i=j=k=0

       while i < len(lefthalf) and j < len(righthalf):
           if lefthalf[i] < righthalf[j]:
               myArray[k]=lefthalf[i]
               i=i+1
           else:
               myArray[k]=righthalf[j]
               j=j+1
           k=k+1

       while i < len(lefthalf):
           myArray[k]=lefthalf[i]
           i=i+1
           k=k+1

       while j < len(righthalf):
            myArray[k]=righthalf[j]
            j=j+1
            k=k+1
