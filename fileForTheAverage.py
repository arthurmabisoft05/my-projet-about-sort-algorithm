def setAverage(average, path):
    '''
        this fct helps 
        for the average calcul
    '''
    fileAverageBs = open(path, "a")
    summary = sum(average)
    result = summary / 100
    fileAverageBs.write(str(result) + "\n")
