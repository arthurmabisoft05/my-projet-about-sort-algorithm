from random import randint
from time import time
from fileOfSorts import quickSort
from fileOfSorts import mergeSort
from fileOfSorts import bubbleSort
from fileForTheAverage import setAverage


arrayWidth = 100000
maxValue = 2**32-1
minValue = 0

myArray = [randint(minValue, maxValue) for i in range(arrayWidth)]
averageArray = []

'''
    i will be using a for loop so that i can create my array's values.
    here is the long format of my loop:
    for i in range(arrayWidth):
        myArray = [random.randint(minValue, maxValue)]+myArray
'''

modeOfSorting = 3

'''
    mode:
    1:mergesort
    2:quicksort
    3:bubblesort
'''

def main() :

    '''
        in my main fct
        i define the path of my file
        i do my sorts
    '''
    if (arrayWidth == 10):
        path = "TD8/10.txt"
    elif(arrayWidth == 100):
        path = "TD8/100.txt"
    elif(arrayWidth == 1000):
        path = "TD8/1000.txt"
    elif(arrayWidth == 10000):
        path = "TD8/10000.txt"
    else:
        path = "TD8/100000.txt"
    file = open(path, 'a')
    if (modeOfSorting == 1):
        file.write("merge sort\n")
        for i in range(0, 100):
            start_time = time()
            mergeSort(myArray)
            print(myArray)
            doneTime = (time() - start_time)
            averageArray.append(doneTime)
        setAverage(averageArray, path)

    elif (modeOfSorting == 2):
        file.write("quick sort\n")
        for i in range(0, 100):
            start_time = time()
            quickSort(myArray, minValue, arrayWidth-1)
            print(myArray)
            doneTime = (time() - start_time)
            averageArray.append(doneTime)
        setAverage(averageArray, path)

    else:
        file.write("bubble sort\n")
        for i in range(0, 100):
            start_time = time()
            bubbleSort(myArray)
            print(myArray)
            doneTime = (time() - start_time)
            averageArray.append(doneTime)
        setAverage(averageArray, path)
    file.close
if __name__ == '__main__':
    main()    
 
